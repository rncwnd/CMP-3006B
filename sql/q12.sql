INSERT INTO `competitor`(`memberID`, `raceID`)
SELECT `memberID`,`raceID`
FROM `member`, `race`, `series`
WHERE `member`.`firstName` = "John"
AND `member`.`lastName` = "Doe"
AND `race`.`raceName` = "Test Race"
AND `series`.`seriesName` = "TEST SERIES"
AND `series`.`seriesYear` = "2018"
AND `race`.`seriesID` = `series`.`seriesID`;
