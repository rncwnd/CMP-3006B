SELECT `firstName`, `lastName`, `grade`, `seriesName`, `seriesYear`, `raceName`, `raceDate`, `position`
FROM `member`, `series`, `race`, `competitor`
WHERE firstName = "John"
AND   lastname = "Doe"
AND   `series`.`seriesID` = `race`.`seriesID`
AND   `competitor`.`memberID` = `member`.`memberID`
AND   `competitor`.`raceID` = `race`.`raceID`
AND   `position` IS NOT NULL
ORDER BY position;
