SELECT `courseName`, `firstName`, `lastName`, `grade`, `seriesName`, `seriesYear`, `raceName`,
`raceDate`, `position`
FROM `course`, `member`, `series`, `race`, `competitor`
WHERE `courseName` = "Basic Canoeing"
AND   `series`.`seriesID` = `race`.`seriesID`
AND   `competitor`.`memberID` = `member`.`memberID`
AND   `competitor`.`raceID` = `race`.`raceID`
AND   `position` IS NOT NULL
ORDER BY `lastName`;
