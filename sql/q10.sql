INSERT INTO `enrolment`(`memberID`,`courseID`)
SELECT `memberID`, `courseID`
FROM `member`, `course`
WHERE `member`.`firstName` = "John"
AND `member`.`lastName` = "Doe"
AND `course`.`courseName` = "Basic Canoeing";
