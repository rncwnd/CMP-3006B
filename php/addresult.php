<html>
<head>
    <title>Canary Canoes Database Interface</title>
    <meta charset='utf-8'>
    <link rel = 'stylesheet' = 'text/css' href = 'style.css'>
</head>

<body>
<?php
    $dbConn   = mysqli_connect('localhost','root','','canary');
    $memberID = mysqli_real_escape_string($dbConn, $_POST['memberID']);
    $raceID   = mysqli_real_escape_string($dbConn, $_POST['raceID']);
    $position = mysqli_real_escape_string($dbConn, $_POST['position']);
    $queryStr = "UPDATE `competitor`
                SET position = '$position'
                WHERE memberID = '$memberID' AND raceID = '$raceID';";

    //Validation on user inputs
    $checkStr = "SELECT * FROM `member` WHERE `member`.`memberID` = '$memberID';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) == 0){
        die("Please use a memeberID that exists!");
    }
    $checkStr = "SELECT * FROM `race` WHERE `race`.`raceID` = '$raceID';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) == 0){
        die("Please use a race that exists!");
    }

    //check for null values and then perform the query
    if($memberID != "" && $raceID != "" && $position != ""){
        echo"<p>Updating member '$memberID's position in race '$raceID' to '$position'</p>";
        $result   = mysqli_query($dbConn, $queryStr);
    }
    else{
        echo"<p>Please ensure that all feilds are not null</p>";
    }
    mysqli_close($dbConn);
?>
</body>
</html>
