<html>
<head>
    <title>Member Dumper</title>
    <meta charset='utf-8'>
    <link rel = 'stylesheet' = 'text/css' href = 'style.css'>
</head>

<body>
<?php
    $dbConn     = mysqli_connect('localhost','root','','canary');
    $raceName   = mysqli_real_escape_string($dbConn, $_POST['raceName']);
    $raceDate   = mysqli_real_escape_string($dbConn, $_POST['raceDate']);
    $seriesName = mysqli_real_escape_string($dbConn, $_POST['seriesName']); 
    $seriesYear = mysqli_real_escape_string($dbConn, $_POST['seriesYear']);
    $queryStr   = "INSERT INTO `race`(`seriesID`, `raceName`, `raceDate`)
                   SELECT `seriesID`, '$raceName', '$raceDate'
                   FROM `series`
                   WHERE `series`.`seriesName` = '$seriesName';";
    $checkStr = "SELECT * FROM `race` WHERE `race`.`raceName` = '$raceName';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) > 0){
        die("A race with this name already exists!");
    }
    $checkStr = "SELECT * FROM `race` WHERE `race`.`raceDate` = '$raceDate';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) > 0){
        die("A race is already happening on that date!");
    }
    if($raceName != "" && $raceDate != "" && $seriesName != "" && $seriesName != ""){
        $result = mysqli_query($dbConn, $queryStr);
        echo"<p> Creating race $raceName with a date of $raceDate,
            as part of series $seriesName in year $seriesYear";
    }
    else{
        echo"<p>Please check that none of your inputs are null</p>";
    }
    mysqli_close($dbConn);
?>
</body>
</html>
