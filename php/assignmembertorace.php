<html>
<head>
    <title>Member Dumper</title>
    <meta charset='utf-8'>
    <link rel = 'stylesheet' = 'text/css' href = 'style.css'>
</head>

<body>
<?php
   $dbConn     = mysqli_connect('localhost','root','','canary');
    $firstName  = mysqli_real_escape_string($dbConn, $_POST['firstName']);
    $lastName   = mysqli_real_escape_string($dbConn, $_POST['lastName']);
    $raceName   = mysqli_real_escape_string($dbConn, $_POST['raceName']);
    $seriesName = mysqli_real_escape_string($dbConn, $_POST['seriesName']);
    $seriesYear = mysqli_real_escape_string($dbConn, $_POST['seriesYear']);
    $queryStr   = "INSERT INTO `competitor`(`memberID`, `raceID`)
                   SELECT `memberID`,`raceID`
                   FROM `member`, `race`, `series`
                   WHERE `member`.`firstName` = '$firstName'
                   AND `member`.`lastName` = '$lastName'
                   AND `race`.`raceName` = '$raceName'
                   AND `series`.`seriesName` = '$seriesName'
                   AND `series`.`seriesYear` = '$seriesYear'
                   AND `race`.`seriesID` = `series`.`seriesID`;";
    //Validate user inputs
    $checkStr = "SELECT * FROM `member` WHERE `member`.`firstName` = '$firstName'
                 AND `member`.`lastName` = '$lastName';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) == 0){
        die("Please use a member that exists!");
    }
    $checkStr = "SELECT * FROM `race` WHERE `race`.`raceName` = '$raceName';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) == 0){
        die("Please use a race that exists!");
    }
    $checkStr = "SELECT * FROM `series` WHERE `series`.`seriesName` = '$seriesName'
                 AND `series`.`seriesYear` = '$seriesYear';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) == 0){
        die("Please use a series that exists!");
    }
    $checkStr = "SELECT `race`.`raceName`
                FROM `race`, `series`
                WHERE `race`.`seriesID` = `series`.`seriesID`
                AND `series`.`seriesName` = '$seriesName'
                AND `race`.`raceName` = '$raceName';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    $result = mysqli_fetch_object($checkQuery);
    if($result->raceName == NULL){
        die("Please use a race that exists in the series");
    }
    

    //check null and then perform the query
    if($firstName != "" && $lastName != "" && $raceName != "" && $seriesName != "" && $seriesYear != ""){
        echo"<p>Assigning $firstName $lastName to $raceName as
             part of $seriesName in $seriesYear";
        $result = mysqli_query($dbConn, $queryStr);
    }
    else{
        echo"<p>Please ensure that all inputs are non-null</p>";
    }
    mysqli_close($dbConn);
?>
<body>
</html>
