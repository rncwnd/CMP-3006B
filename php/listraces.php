<html>
<head>
    <title>Canary Canoes database Interface</title>
    <link rel ="stylesheet" type="text/css" href="style.css">
    <meta charset="utf-8">
<head>

<body>
<?php
    $title     = "List Races by Competitor";
    $dbConn    = mysqli_connect('localhost','root','','canary');
    $firstName = mysqli_real_escape_string($dbConn, $_POST['firstName']);
    $lastName  = mysqli_real_escape_string($dbConn, $_POST['lastName']);
    $queryStr  = "SELECT `firstName`, `lastName`, `grade`, `seriesName`, `seriesYear`, `raceName`, `raceDate`, `position`
                  FROM `member`, `series`, `race`, `competitor`
                  WHERE firstName = '$firstName'
                  AND   lastname = '$lastName'
                  AND   `series`.`seriesID` = `race`.`seriesID`
                  AND   `competitor`.`memberID` = `member`.`memberID`
                  AND   `competitor`.`raceID` = `race`.`raceID`
                  AND   `position` IS NOT NULL
                  ORDER BY position;";
    if($firstName != "" && $lastName != ""){
        $result = mysqli_query($dbConn, $queryStr);
        if(mysqli_num_rows($result) == 0){
            echo"<p>That person does not exist!</p>";
        }
        else{
        echo"<h3>Listing all races that '$firstName' '$lastName' has participated in</h3>";
        echo"<table>";
        echo"<tr>";
        echo"    <th>firstName</th>";
        echo"    <th>lastName</th>";
        echo"    <th>grade</th>";
        echo"    <th>seriesName</th>";
        echo"    <th>seriesYear</th>";
        echo"    <th>raceName</th>";
        echo"    <th>raceDate</th>";
        echo"    <th>position</th>";
        echo"<tr>";
        while($row = mysqli_fetch_assoc($result))
        {
            echo"<tr>";
                echo"<td>".$row['firstName']."</td>";
                echo"<td>".$row['lastName']."</td>";
                echo"<td>".$row['grade']."</td>";
                echo"<td>".$row['seriesName']."</td>";
                echo"<td>".$row['seriesYear']."</td>";
                echo"<td>".$row['raceName']."</td>";
                echo"<td>".$row['raceDate']."</td>";
                echo"<td>".$row['position']."</td>";
           echo"</tr>";
        }
        }
    }
    else
        echo"<p>Please enter a first name and last name</p>";
?>
</body>
