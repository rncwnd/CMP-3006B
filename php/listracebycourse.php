<html>
<head>
    <title>Canary Canoes database Interface</title>
    <link rel ="stylesheet" type="text/css" href="style.css">
    <meta charset="utf-8">
<head>

<body>
<?php
    $title      = "List Races by Course";
    $dbConn     = mysqli_connect('localhost','root','','canary');
    $courseName = mysqli_real_escape_string($dbConn, $_POST['courseName']);
    $queryStr   = "SELECT `courseName`, `firstName`, `lastName`, `grade`, 
                  `seriesName`, `seriesYear`, `raceName`,
                  `raceDate`, `position`
                  FROM `course`, `member`, `series`, `race`, `competitor`
                  WHERE `courseName` = '$courseName'
                  AND   `series`.`seriesID` = `race`.`seriesID`
                  AND   `competitor`.`memberID` = `member`.`memberID`
                  AND   `competitor`.`raceID` = `race`.`raceID`
                  AND   `position` IS NOT NULL
                  ORDER BY `lastName`;";
    $checkStr = "SELECT * FROM `course` WHERE `course`.`courseName` = '$courseName';";
    $checkQuery = mysqli_query($dbConn, $checkStr);
    if(mysqli_num_rows($checkQuery) == 0){
        die("Please use a course that exists");
    }
    if($courseName != ""){
        $result     = mysqli_query($dbConn, $queryStr);
        echo"<h3>Listing all races that members of '$courseName' have participated in</h3>";
        echo"<table>";
        echo"<tr>";
        echo"    <th>courseName</th>";
        echo"    <th>firstName</th>";
        echo"    <th>lastName</th>";
        echo"    <th>grade</th>";
        echo"    <th>seriesName</th>";
        echo"    <th>seriesYear</th>";
        echo"    <th>raceName</th>";
        echo"    <th>raceDate</th>";
        echo"    <th>position</th>";
        echo"<tr>";
        while($row = mysqli_fetch_assoc($result))
        {
            echo"<tr>";
                echo"<td>".$row['courseName']."</td>";
                echo"<td>".$row['firstName']."</td>";
                echo"<td>".$row['lastName']."</td>";
                echo"<td>".$row['grade']."</td>";
                echo"<td>".$row['seriesName']."</td>";
                echo"<td>".$row['seriesYear']."</td>";
                echo"<td>".$row['raceName']."</td>";
                echo"<td>".$row['raceDate']."</td>";
                echo"<td>".$row['position']."</td>";
            echo"</tr>";
        }
    }
    else
        echo"<p>Please enter a course name</p>";
?>
</body>
