# CMP-3006B

# Decisions

* Including a header with a php file adds workload to the server.
Prefer including header boilerplate to using php includes. HTML5 imports are not supported in
all browsers so cannot be used.
* Only use PHP when the code needs to do processing on the server rather than using it to generate
something that already can be done with HTML, this improves code readability.
* Try to optimise SQL statements.
* Ensure that all user input is escaped as to not expose ourselves to SQLi.
* Ensure that invalid user input results in a fast exit of the application instead
of causing runtime errors.
